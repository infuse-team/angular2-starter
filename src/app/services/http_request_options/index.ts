import { Injectable } from '@angular/core';
import { BaseRequestOptions } from '@angular/http';

@Injectable()
export class CustomRequestOptions extends BaseRequestOptions {

    /*
    *
    * Essentially an HTTP interceptor.
    * 1) user = 'currentUser' from LocalStorage
    * 2) Gets Token and Username from user
    * 3) Appends token and username to http headers
    *
    * Ideally, the user object would live in a USer Service, and this would
    * reference that service
    *
    * */

    constructor() {
        super();
        let user = JSON.parse(window.localStorage.getItem('currentUser'));
        let token = user ? user.token : null;
        let username = user ? user.username : null;

        this.headers.append('token', token);
        this.headers.append('username', username);
    }
}
