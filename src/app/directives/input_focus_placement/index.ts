import { Directive, ElementRef, HostListener, Renderer } from '@angular/core';

@Directive({
    selector: '[inputFocusPlacement]'
})

export class InputFocusPlacement {

    /*
    *
    * onFocus, cursor always defaults to end of input
    *
    */

    constructor(private el: ElementRef, private renderer: Renderer) {}

    @HostListener('focus', ['$event.target'])
    onFocus(target) {

        let endIndex = target.value.length;
        if (target.setSelectionRange) {
            setTimeout(function() {
                target.setSelectionRange(endIndex, endIndex);
            }, 0);
        }
    }
}
