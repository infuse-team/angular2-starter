// taken from http://stackoverflow.com/a/35261193

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'MapToIterable'
})
export class MapToIterable implements PipeTransform {
    transform(obj: Object): Array<any> {
        let a = [];
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                a.push({key: key, val: obj[key]});
            }
        }
        return a;
    }
}
