import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'my-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private testObj: Object;

  constructor() {
    // Do stuff
    this.testObj = {
      key1: 'value1',
      key2: 'value2',
      key3: 'value3',
      key4: 'value4'
    };
  }

  ngOnInit() {
    console.log('Hello Home');
  }

}
